defmodule PhoenixTrello.Repo.Migrations.AddPositionCard do
  use Ecto.Migration

  def change do
        alter table(:cards) do
          add :position, :integer
        end
  end
end
